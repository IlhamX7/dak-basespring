/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.basespring.controller;

import com.spring.basespring.model.LogActivity;
import com.spring.basespring.spring.SpringInitializer;
import com.spring.basespring.utility.Constants;
import com.spring.basespring.utility.ResponseWebServiceContainer;
import java.text.ParseException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.jpos.space.Space;
import org.jpos.space.SpaceFactory;
import org.jpos.transaction.Context;
import org.jpos.util.Log;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Andri D Septian
 */
@RestController
public class MainController {

    Log log = Log.getLog("Q2", getClass().getName());
    
    ResponseWebServiceContainer rwsc = new ResponseWebServiceContainer();
    JSONObject jso = new JSONObject();

    @RequestMapping(value = "/test/{service_type}", method = RequestMethod.POST)
    public String allProcess(@RequestBody(required = false) String body, @PathVariable("service_type") String path,
            HttpServletRequest request) throws ParseException {

        LogActivity logActivity = new LogActivity();

        try {
            Context ctx = new Context();
            Space space = SpaceFactory.getSpace();
            JSONObject json = new JSONObject(body);
            logActivity.setRequest(body.toString());
            logActivity.setDtm_created_msg(new Date());

            logActivity = SpringInitializer.getLogActivityDao().saveOrUpdate(logActivity);

            ctx.put(Constants.REQ_BODY, json);
            ctx.put(Constants.PATH, path);
            ctx.put(Constants.LOG, logActivity);

            space.out("test-trxmgr", ctx, 60000);
            Context ctx_response = (Context) space.in(logActivity.getId(), 60000);

            SpringInitializer.getLogActivityDao().saveOrUpdate(logActivity);

            if (true) {
                rwsc = (ResponseWebServiceContainer) ctx_response.get(Constants.BODY_RESPONSE);
                //logActivity.setResponse((String) ctx_response.get(Constants.STATUS));
                logActivity.setResponse(rwsc.jsonToString());
                SpringInitializer.getLogActivityDao().saveOrUpdate(logActivity);
                return rwsc.jsonToString();
            } else {
                logActivity.setResponse("TIMEOUT");
                SpringInitializer.getLogActivityDao().saveOrUpdate(logActivity);
                jso.put("response", "TIMEOUT");
                rwsc = new ResponseWebServiceContainer(jso);
                return rwsc.jsonToString();
            }

        } catch (Exception e) {
//            e.printStackTrace();
//            jso.put("response", "ERROR, NoResultException");
//            jso.put("catch", "MainController");
//            rwsc = new ResponseWebServiceContainer(jso);
//            return rwsc.jsonToString();

            e.printStackTrace();
            logActivity.setResponse("Error");
            return logActivity.getResponse();
        }
    }

    @RequestMapping(value = "/test/{service_type}", method = RequestMethod.GET)
    public String getProcess(@RequestBody(required = false) String body, @PathVariable("service_type") String path,
                             HttpServletRequest request) throws ParseException {

        LogActivity logActivity = new LogActivity();


        try {
            Context ctx = new Context();
            Space space = SpaceFactory.getSpace();
            logActivity.setRequest("");
            logActivity.setDtm_created_msg(new Date());

            logActivity = SpringInitializer.getLogActivityDao().saveOrUpdate(logActivity);

            ctx.put(Constants.PATH, path);
            ctx.put(Constants.LOG, logActivity);

            space.out("test-trxmgr", ctx, 60000);
            Context ctx_response = (Context) space.in(logActivity.getId(), 60000);

            SpringInitializer.getLogActivityDao().saveOrUpdate(logActivity);

            if (true) {
                rwsc = (ResponseWebServiceContainer) ctx_response.get(Constants.BODY_RESPONSE);
                //logActivity.setResponse((String) ctx_response.get(Constants.STATUS));
                logActivity.setResponse(rwsc.jsonToString());
                SpringInitializer.getLogActivityDao().saveOrUpdate(logActivity);
                return rwsc.jsonToString();
            } else {
                logActivity.setResponse("TIMEOUT");
                SpringInitializer.getLogActivityDao().saveOrUpdate(logActivity);
                jso.put("response", "TIMEOUT");
                rwsc = new ResponseWebServiceContainer(jso);
                return rwsc.jsonToString();
            }

        } catch (Exception e) {
//            e.printStackTrace();
//            jso.put("response", "ERROR, NoResultException");
//            jso.put("catch", "MainController");
//            rwsc = new ResponseWebServiceContainer(jso);
//            return rwsc.jsonToString();

            e.printStackTrace();
            logActivity.setResponse("Error");
            return logActivity.getResponse();
        }
    }


    @RequestMapping(value = "/xml/{service_type}", method = RequestMethod.POST)
    public String allProcessXml(@RequestBody(required = false) String body, @PathVariable("service_type") String path,
            HttpServletRequest request) throws ParseException {
        try {
            System.out.println("===============> XML Body <===============");
            System.out.println(body);
            System.out.println("---------------^ XML Body ^---------------");
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
