/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.basespring.spring;

import com.spring.basespring.dao.LogActivityDao;
import com.spring.basespring.dao.UserDataDao;
import com.spring.basespring.dao.UserDetailDao;
import com.spring.basespring.dao.MhsDataDao;
import javax.naming.ConfigurationException;
import org.jpos.util.Log;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 *
 * @author Andri D Septian
 */
public class SpringInitializer {

    Log log = Log.getLog("Q2", getClass().getName());
    
    public static UserDataDao userDataDao;
    public static UserDetailDao userDetailDao;
    public static LogActivityDao logActivityDao;
    public static MhsDataDao mhsDataDao;

    public static UserDataDao getUserDataDao() {
        return userDataDao;
    }

    public static void setUserDataDao(UserDataDao userDataDao) {
        SpringInitializer.userDataDao = userDataDao;
    }

    public static UserDetailDao getUserDetailDao() {
        return userDetailDao;
    }

    public static void setUserDetailDao(UserDetailDao userDetailDao) {
        SpringInitializer.userDetailDao = userDetailDao;
    }

    public static LogActivityDao getLogActivityDao() {
        return logActivityDao;
    }

    public static void setLogActivityDao(LogActivityDao logActivityDao) {
        SpringInitializer.logActivityDao = logActivityDao;
    }

    public static MhsDataDao getMhsDataDao() {
        return mhsDataDao;
    }

    public static void setMhsDataDao(MhsDataDao mhsDataDao) {
        SpringInitializer.mhsDataDao = mhsDataDao;
    }

    public void initService() throws ConfigurationException {

        ApplicationContext context = new FileSystemXmlApplicationContext("/src/main/resources/ApplicationContext.xml");
//        ApplicationContext context = new FileSystemXmlApplicationContext("ApplicationContext.xml");

//        setmUserDao(context.getBean("MUserDao", MUserDao.class));
        setUserDataDao(context.getBean("UserDataDao", UserDataDao.class));
        setUserDetailDao(context.getBean("UserDetailDao", UserDetailDao.class));
        setLogActivityDao(context.getBean("LogActivityDao", LogActivityDao.class));
        setMhsDataDao(context.getBean("MhsDataDao", MhsDataDao.class));

        log.info("Init DB has Started");
        log.info("Service Started");
    }
}
