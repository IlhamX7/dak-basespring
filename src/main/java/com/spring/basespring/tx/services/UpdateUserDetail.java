package com.spring.basespring.tx.services;

import com.spring.basespring.model.UserData;
import com.spring.basespring.model.UserDetail;
import com.spring.basespring.spring.SpringInitializer;
import com.spring.basespring.utility.Constants;
import com.spring.basespring.utility.ResponseWebServiceContainer;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

import javax.persistence.NoResultException;
import java.io.Serializable;

public class UpdateUserDetail implements TransactionParticipant {
    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer rwsc = new ResponseWebServiceContainer();
    JSONObject jso = new JSONObject();

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;

        try {
            JSONObject json = (JSONObject) ctx.get(Constants.REQ_BODY);

            UserDetail userDetail = SpringInitializer.getUserDetailDao().findUserById(json.getLong("id"));

            String oldusername = userDetail.getUsername();

            userDetail.setUsername(json.getString("username"));

            SpringInitializer.getUserDetailDao().saveOrUpdate(userDetail);

            UserData userData = SpringInitializer.getUserDataDao().findUserByUsername(oldusername);
            userData.setUsername(json.getString("username"));

            SpringInitializer.getUserDataDao().saveOrUpdate(userData);

            jso.put("Status", "Changed");
            rwsc = new ResponseWebServiceContainer(jso);
            ctx.put(Constants.BODY_RESPONSE, rwsc);

        } catch (NoResultException e) {
            e.printStackTrace();
            ctx.put(Constants.BODY_RESPONSE, "Update Detail ERROR");
        }

    }

    @Override
    public void abort(long id, Serializable srlzbl) {

    }
}
