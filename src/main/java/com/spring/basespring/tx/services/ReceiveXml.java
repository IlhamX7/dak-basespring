/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.basespring.tx.services;

import java.io.Serializable;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilderFactory;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/**
 *
 * @author Andri D Septian
 */
public class ReceiveXml implements TransactionParticipant{
    
    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }
    
    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        StringBuffer response = new StringBuffer();
        
        try {
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(response.toString())));
            System.out.println(doc);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public void abort(long id, Serializable srlzbl) {
        
    }
    
}
