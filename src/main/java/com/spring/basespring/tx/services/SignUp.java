/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.basespring.tx.services;

import com.spring.basespring.model.UserData;
import com.spring.basespring.model.UserDetail;
import com.spring.basespring.spring.SpringInitializer;
import com.spring.basespring.utility.Constants;
import com.spring.basespring.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.NoResultException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Andri D Septian
 */
public class SignUp implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());
    
    ResponseWebServiceContainer rwsc = new ResponseWebServiceContainer();
    JSONObject jso = new JSONObject();

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;

        try {
            UserData regDat = new UserData();
            UserDetail regDetail =  new UserDetail();
            JSONObject json = (JSONObject) ctx.get(Constants.REQ_BODY);

            
            String  username = json.getString("username");
            log.info(username);
            String password = json.getString("password");
            log.info(password);
            
            String fullname = json.getString("fullname");
            String contact = json.getString("contact");
            

            UserData checkDat = SpringInitializer.getUserDataDao().findUserByUsername(username);
            UserDetail checkDetail = SpringInitializer.getUserDetailDao().findUserDetByUsername(username);

            if (checkDat == null && checkDetail == null) {
                regDat.setUsername(username);
                regDat.setPassword(password);
                regDetail.setUsername(regDat.getUsername());
                regDetail.setId(regDat.getId());
                regDetail.setFullname(fullname);
                regDetail.setContact(contact);

                log.info("__DEBUG " + json.getString("password"));
                regDat.setDateCreated(new Date());
                regDat.setDateLastLogin(new Date());
                regDat.setStatus("0");

               UserData user= SpringInitializer.getUserDataDao().saveOrUpdate(regDat);
                SpringInitializer.getUserDetailDao().saveOrUpdate(regDetail);

                jso.put("Status", "Registered");
                rwsc = new ResponseWebServiceContainer(jso);
                ctx.put(Constants.BODY_RESPONSE, rwsc);
                
            } else {
                jso.put("Status", "Username already exist");
                rwsc = new ResponseWebServiceContainer(jso);
                ctx.put(Constants.BODY_RESPONSE, rwsc);
            }

        } catch (NoResultException e) {
            e.printStackTrace();
            ctx.put(Constants.BODY_RESPONSE, "Sign Up ERROR");
        }
    }

    @Override
    public void abort(long id, Serializable srlzbl) {

    }

}
