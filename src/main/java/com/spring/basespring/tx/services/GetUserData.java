package com.spring.basespring.tx.services;

import com.spring.basespring.model.UserData;
import com.spring.basespring.spring.SpringInitializer;
import com.spring.basespring.utility.Constants;
import com.spring.basespring.utility.ResponseWebServiceContainer;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

public class GetUserData implements TransactionParticipant {
    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer rwsc = new ResponseWebServiceContainer();
    JSONObject jso = new JSONObject();

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        try{
            List<UserData> userdata = SpringInitializer.getUserDataDao().getAllUserData();
            JSONArray jsonArray = new JSONArray();
            JSONObject object;
            for(UserData user:userdata){
                object = new JSONObject();
                object.put("id",user.getId());
                object.put("username",user.getUsername());
                object.put("password",user.getPassword());
                jsonArray.put(object);
            }

            ctx.put(Constants.BODY_RESPONSE, new ResponseWebServiceContainer(new JSONObject().put("data", jsonArray)));
        }catch (Exception e){
            e.printStackTrace();
            ctx.put(Constants.BODY_RESPONSE, "Login Error");
        }
    }

    @Override
    public void abort(long id, Serializable srlzbl) {

    }
}
