package com.spring.basespring.tx.services;

import com.spring.basespring.spring.SpringInitializer;
import com.spring.basespring.utility.Constants;
import com.spring.basespring.utility.ResponseWebServiceContainer;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.Serializable;
import java.util.List;

import com.spring.basespring.model.MhsData;

import javax.persistence.NoResultException;

/**
 *
 * @author Ilham
 */
public class GetMhsData implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer rwsc = new ResponseWebServiceContainer();
    JSONObject jso = new JSONObject();

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;

        try {
            List<MhsData> mhsdata = SpringInitializer.getMhsDataDao().getAllMhsData();
            JSONArray jsonArray = new JSONArray();
            JSONObject object;
            for(MhsData mhs:mhsdata){
                object = new JSONObject();
                object.put("id",mhs.getId());
                object.put("nim",mhs.getNim());
                object.put("name",mhs.getName());
                object.put("dob",mhs.getDob());
                object.put("address",mhs.getAddress());
                jsonArray.put(object);
            }
            ctx.put(Constants.BODY_RESPONSE, new ResponseWebServiceContainer(new JSONObject().put("data",jsonArray)));
        } catch (NoResultException e) {
            e.printStackTrace();
            ctx.put(Constants.BODY_RESPONSE, "Login Error");
        }
    }

    @Override
    public void abort(long id, Serializable srlzbl) {

    }
}
