/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.basespring.tx.services;

import com.spring.basespring.model.UserData;
import com.spring.basespring.spring.SpringInitializer;
import com.spring.basespring.utility.Constants;
import com.spring.basespring.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import javax.persistence.NoResultException;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Andri D Septian
 */
public class CheckUser implements TransactionParticipant{
    
    Log log = Log.getLog("Q2", getClass().getName());
    
    ResponseWebServiceContainer rwsc = new ResponseWebServiceContainer();
    JSONObject jso = new JSONObject();
    
    @Override
    public int prepare(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        
        try {
            
            JSONObject json = (JSONObject) ctx.get(Constants.REQ_BODY);
            String username = json.getString("username");
            
            UserData userdat = SpringInitializer.getUserDataDao().findUserByUsername(username);
            
            if(userdat != null) {
                ctx.put(Constants.DATA.USER, userdat);
                ctx.put(Constants.REQ_BODY, json);
                ctx.put(Constants.BODY_RESPONSE, rwsc);
                
                return PREPARED | NO_JOIN;
            }
            else {
                ctx.put(Constants.STATUS, "NOT FOUND");
                jso.put(Constants.STATUS, "NOT FOUND");
                rwsc = new ResponseWebServiceContainer(jso);
                ctx.put(Constants.BODY_RESPONSE, rwsc);
                return ABORTED | NO_JOIN;
            }
            
        } catch (NoResultException e) {
            e.printStackTrace();
            ctx.put(Constants.STATUS, "NoResultException");
            jso.put(Constants.STATUS, "NoResultException");
            rwsc = new ResponseWebServiceContainer(jso);
            ctx.put(Constants.BODY_RESPONSE, rwsc);
            return  ABORTED | NO_JOIN;
        }
    }
    
    @Override
    public void commit(long id, Serializable srlzbl) {
        
    }
    
    @Override
    public void abort(long id, Serializable srlzbl) {
        
    }
}
