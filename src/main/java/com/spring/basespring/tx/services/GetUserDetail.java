package com.spring.basespring.tx.services;

import com.spring.basespring.model.UserDetail;
import com.spring.basespring.spring.SpringInitializer;
import com.spring.basespring.utility.Constants;
import com.spring.basespring.utility.ResponseWebServiceContainer;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

public class GetUserDetail implements TransactionParticipant {
    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer rwsc = new ResponseWebServiceContainer();
    JSONObject jso = new JSONObject();

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        try{
            List<UserDetail> userdetail = SpringInitializer.getUserDetailDao().getAllUserDetail();
            JSONArray jsonArray = new JSONArray();
            JSONObject object;
            for(UserDetail user:userdetail){
                object = new JSONObject();
                object.put("id",user.getId());
                object.put("username",user.getUsername());
                object.put("fullname",user.getFullname());
                object.put("contact_number",user.getContact());
                jsonArray.put(object);
            }
            ctx.put(Constants.BODY_RESPONSE, new ResponseWebServiceContainer(new JSONObject().put("data",jsonArray)));
        }catch (Exception e){
            e.printStackTrace();
            ctx.put(Constants.BODY_RESPONSE, "Login Error");
        }
    }

    @Override
    public void abort(long id, Serializable srlzbl) {

    }
}
