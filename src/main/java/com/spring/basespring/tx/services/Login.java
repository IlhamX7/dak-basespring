/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.basespring.tx.services;

import com.spring.basespring.model.UserData;
import com.spring.basespring.spring.SpringInitializer;
import com.spring.basespring.utility.Constants;
import com.spring.basespring.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.Date;

import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

/**
 *
 * @author Andri D Septian
 */
public class Login implements TransactionParticipant{
    
    Log log = Log.getLog("Q2", getClass().getName());
    
    ResponseWebServiceContainer rwsc = new ResponseWebServiceContainer();
    JSONObject jso = new JSONObject();
    
    
    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }
    
    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;
        
        
        try {
            UserData login = (UserData) ctx.get(Constants.DATA.USER);
            JSONObject json = (JSONObject) ctx.get(Constants.REQ_BODY);
            
            if(login.getPassword().equals(json.getString("password"))){
                login.setStatus("1");
                login.setDateLastLogin(new Date());
                SpringInitializer.getUserDataDao().saveOrUpdate(login);
                ctx.put(Constants.STATUS, "Sukses");
                jso.put(Constants.STATUS, "LOGIN");
                rwsc = new ResponseWebServiceContainer(jso);
                ctx.put(Constants.BODY_RESPONSE, rwsc);
            }
            else{
                ctx.put(Constants.STATUS, "Gagal");
                jso.put(Constants.STATUS, "FAILED");
                rwsc = new ResponseWebServiceContainer(jso);
                ctx.put(Constants.BODY_RESPONSE, rwsc);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            ctx.put(Constants.BODY_RESPONSE, "Login Error");
        }
    }
    
    @Override
    public void abort(long id, Serializable srlzbl) {
        
    }
    
}
