package com.spring.basespring.tx.services;

import com.spring.basespring.model.MhsData;
import com.spring.basespring.spring.SpringInitializer;
import com.spring.basespring.utility.Constants;
import com.spring.basespring.utility.ResponseWebServiceContainer;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

import javax.persistence.NoResultException;
import java.io.Serializable;

public class UpdateMhs implements TransactionParticipant {
    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer rwsc = new ResponseWebServiceContainer();
    JSONObject jso = new JSONObject();

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;

        try {
            JSONObject json = (JSONObject) ctx.get(Constants.REQ_BODY);

            MhsData mhsData = SpringInitializer.getMhsDataDao().findMhsById(json.getLong("id"));

            mhsData.setName(json.getString("name"));

            SpringInitializer.getMhsDataDao().saveOrUpdate(mhsData);

            jso.put("Status", "Changed");
            rwsc = new ResponseWebServiceContainer(jso);
            ctx.put(Constants.BODY_RESPONSE, rwsc);

        } catch (NoResultException e) {
            e.printStackTrace();
            ctx.put(Constants.BODY_RESPONSE, "Update Data ERROR");
        }

    }

    @Override
    public void abort(long id, Serializable srlzbl) {

    }
}
