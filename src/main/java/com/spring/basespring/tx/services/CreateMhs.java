package com.spring.basespring.tx.services;

import com.spring.basespring.model.MhsData;
import com.spring.basespring.spring.SpringInitializer;
import com.spring.basespring.utility.Constants;
import com.spring.basespring.utility.ResponseWebServiceContainer;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

import javax.persistence.NoResultException;
import java.io.Serializable;

public class CreateMhs implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer rwsc = new ResponseWebServiceContainer();
    JSONObject jso = new JSONObject();

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;

        try {
            MhsData regDat = new MhsData();
            JSONObject json = (JSONObject) ctx.get(Constants.REQ_BODY);

            Integer nim = json.getInt("nim");
            log.info(nim);
            String name = json.getString("name");
            log.info(name);
            String dob = json.getString("dob");
            log.info(dob);
            String address = json.getString("address");
            log.info(address);

            MhsData checkDat = SpringInitializer.getMhsDataDao().findMhsByName(name);
            MhsData checkDetail = SpringInitializer.getMhsDataDao().findMhsByName(name);

            if (checkDat == null && checkDetail == null) {
                regDat.setNim(nim);
                regDat.setName(name);
                regDat.setDob(dob);
                regDat.setAddress(address);

                SpringInitializer.getMhsDataDao().saveOrUpdate(regDat);

                jso.put("Status", "Created");
                rwsc = new ResponseWebServiceContainer(jso);
                ctx.put(Constants.BODY_RESPONSE, rwsc);

            } else {
                jso.put("Status", "Name already exist");
                rwsc = new ResponseWebServiceContainer(jso);
                ctx.put(Constants.BODY_RESPONSE, rwsc);
            }

        } catch (NoResultException e) {
            e.printStackTrace();
            ctx.put(Constants.BODY_RESPONSE, "Create Data ERROR");
        }
    }

    @Override
    public void abort(long id, Serializable srlzbl) {

    }
}
