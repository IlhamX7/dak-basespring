/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.basespring.tx.services;

import com.spring.basespring.model.UserData;
import com.spring.basespring.spring.SpringInitializer;
import com.spring.basespring.utility.Constants;
import com.spring.basespring.utility.ResponseWebServiceContainer;
import java.io.Serializable;
import java.util.Date;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.json.JSONObject;

/**
 *
 * @author Andri D Septian
 */
public class Logout implements TransactionParticipant {

    ResponseWebServiceContainer rwsc = new ResponseWebServiceContainer();
    JSONObject jso = new JSONObject();

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;

        try {

            UserData logout = (UserData) ctx.get(Constants.DATA.USER);
            JSONObject json = (JSONObject) ctx.get(Constants.REQ_BODY);

            String username = json.getString("username");

            logout = SpringInitializer.getUserDataDao().findUserByUsername(username);

            if (logout.getUsername().equals(username)) {
                logout.setStatus("0");
                logout.setDateLastLogout(new Date());
                SpringInitializer.getUserDataDao().saveOrUpdate(logout);
                jso.put("Status", "Logged Out");
                rwsc = new ResponseWebServiceContainer(jso);
                ctx.put(Constants.BODY_RESPONSE, rwsc);

            } else {
                jso.put("Status", "Username Not Found");
                rwsc = new ResponseWebServiceContainer(jso);
                ctx.put(Constants.BODY_RESPONSE, rwsc);

            }

        } catch (Exception e) {

        }
    }

    @Override
    public void abort(long id, Serializable srlzbl) {

    }
}
