package com.spring.basespring.tx.services;

import com.spring.basespring.model.UserData;
import com.spring.basespring.model.UserDetail;
import com.spring.basespring.spring.SpringInitializer;
import com.spring.basespring.utility.Constants;
import com.spring.basespring.utility.ResponseWebServiceContainer;
import org.jpos.transaction.Context;
import org.jpos.transaction.TransactionParticipant;
import org.jpos.util.Log;
import org.json.JSONObject;

import javax.persistence.NoResultException;
import java.io.Serializable;

public class DeleteUserData implements TransactionParticipant {

    Log log = Log.getLog("Q2", getClass().getName());

    ResponseWebServiceContainer rwsc = new ResponseWebServiceContainer();
    JSONObject jso = new JSONObject();

    @Override
    public int prepare(long id, Serializable srlzbl) {
        return PREPARED;
    }

    @Override
    public void commit(long id, Serializable srlzbl) {
        Context ctx = (Context) srlzbl;

        try {
            JSONObject json = (JSONObject) ctx.get(Constants.REQ_BODY);

            UserData userData = SpringInitializer.getUserDataDao().findUserById(json.getLong("id"));

            SpringInitializer.getUserDataDao().deleteUserById(userData.getId());

            UserDetail userDetail = SpringInitializer.getUserDetailDao().findUserById(json.getLong("id"));

            SpringInitializer.getUserDetailDao().deleteUserById(userDetail.getId());

            jso.put("Status", "Deleted");
            rwsc = new ResponseWebServiceContainer(jso);
            ctx.put(Constants.BODY_RESPONSE, rwsc);

        } catch (NoResultException e) {
            e.printStackTrace();
            ctx.put(Constants.BODY_RESPONSE, "Delete Data ERROR");
        }
    }

    @Override
    public void abort(long id, Serializable srlzbl) {

    }
}
