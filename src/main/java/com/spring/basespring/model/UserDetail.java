/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.basespring.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author Andri D Septian
 */

@Entity(name = "UserDetail")
@Table(name = "user_detail")
public class UserDetail {
//    @Id
//    //@ManyToOne
//    @JoinColumn(name = "id", nullable = false)
//    private Long id;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_userdet_seq")
    @SequenceGenerator(name = "id_userdet_seq", sequenceName = "id_userdet_seq")
    @Column(name = "id", nullable = false)
    private Long id;
    
    //@ManyToOne
    @JoinColumn(name = "username", nullable = false)
    private String Username;
    
    @Column(name = "fullname", nullable = false)
    private String Fullname;
    
    @Column(name = "contact_number", nullable = false)
    private String Contact;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getFullname() {
        return Fullname;
    }

    public void setFullname(String Fullname) {
        this.Fullname = Fullname;
    }

    public String getContact() {
        return Contact;
    }

    public void setContact(String Contact) {
        this.Contact = Contact;
    }
}
