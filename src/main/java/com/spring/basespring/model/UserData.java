/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.basespring.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;



/**
 *
 * @author Andri D Septian
 */
@Entity(name = "UserData")
@Table(name = "user_data")
public class UserData {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_user_seq")
    @SequenceGenerator(name = "id_user_seq", sequenceName = "id_user_seq")
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Column(name = "username", nullable = false)
    private String Username;
    
    @Column(name = "password", nullable = false)
    private String Password;
    
    @Column(name = "status", nullable = false)
    private String Status;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "dtm_created", nullable = true)
    private Date DateCreated;
     
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "dtm_last_login", nullable = true)
    private Date DateLastLogin;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "dtm_last_logout", nullable = true)
    private Date DateLastLogout;

    public Date getDateLastLogout() {
        return DateLastLogout;
    }

    public void setDateLastLogout(Date DateLastLogout) {
        this.DateLastLogout = DateLastLogout;
    }

    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public Date getDateCreated() {
        return DateCreated;
    }

    public void setDateCreated(Date DateCreated) {
        this.DateCreated = DateCreated;
    }

    public Date getDateLastLogin() {
        return DateLastLogin;
    }

    public void setDateLastLogin(Date DateLastLogin) {
        this.DateLastLogin = DateLastLogin;
    }
    
    
    
}
