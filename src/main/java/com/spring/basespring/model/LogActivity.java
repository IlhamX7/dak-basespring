/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.basespring.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Type;

/**
 *
 * @author Andri D Septian
 */

@Entity(name = "LogActivity")
@Table(name = "log_activity")
public class LogActivity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_logact_seq")
    @SequenceGenerator(name = "id_logact_seq", sequenceName = "id_logact_seq")
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Column(name = "request", nullable = false)
    private String request;
    
    @Column(name = "response", nullable = true)
    @Type(type = "text")
    private String response;
    
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    @Column(name = "dtm_created_msg", nullable = true)
    private Date dtm_created_msg;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public Date getDtm_created_msg() {
        return dtm_created_msg;
    }

    public void setDtm_created_msg(Date dtm_created_msg) {
        this.dtm_created_msg = dtm_created_msg;
    }
    
    
}
