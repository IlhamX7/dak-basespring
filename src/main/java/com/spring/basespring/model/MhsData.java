package com.spring.basespring.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;



/**
 *
 * @author Ilham
 */
@Entity(name = "MhsData")
@Table(name = "mhs_data")
public class MhsData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "id_mhs_seq")
    @SequenceGenerator(name = "id_mhs_seq", sequenceName = "id_mhs_seq")
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "nim", nullable = false)
    private Integer Nim;

    @Column(name = "name", nullable = false)
    private String Name;

    @Column(name = "dob", nullable = false)
    private String Dob;

    @Column(name = "address", nullable = true)
    private String Address;

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public Integer getNim() { return Nim; }

    public void setNim(Integer Nim) { this.Nim = Nim; }

    public String getName() { return Name; }

    public void setName(String Name) { this.Name = Name; }

    public String getDob() { return Dob; }

    public void setDob(String Dob) { this.Dob = Dob; }

    public String getAddress() { return Address; }

    public void setAddress(String Address) { this.Address = Address; }
    
}
