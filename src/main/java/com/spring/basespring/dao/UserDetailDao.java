/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.basespring.dao;

import com.spring.basespring.model.UserDetail;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * @author Andri D Septian
 */
@Repository(value = "UserDetailDao")
@Transactional
public class UserDetailDao extends Dao {
    
    public UserDetail findUserDetByUsername(String username){
        try {
            UserDetail userdetail = (UserDetail) em.createQuery("SELECT a FROM UserDetail a WHERE a.Username = :username")
                    .setParameter("username", username)
                    .getSingleResult();
            return userdetail;
        } catch (NoResultException e) {
            return null;
        }
    }

    public UserDetail saveOrUpdate(UserDetail userdetail) {
        if (userdetail.getId() == null) {
            em.persist(userdetail);
            System.out.println("persist / saved");
        } else {
            em.merge(userdetail);
            System.out.println("Merge / updated");
        }
        return userdetail;
    }

    public UserDetail findUserByUsername( String username ) {
        try {
            return (UserDetail) em.createQuery("SELECT a FROM UserDetail a WHERE username =:username").setParameter("username", username).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<UserDetail> getAllUserDetail() {
        try {
            return (List<UserDetail>) em.createQuery("SELECT a FROM UserDetail a").getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    public UserDetail findUserById( Long idUser ) {
        try {
            return (UserDetail) em.createQuery("SELECT a FROM UserDetail a WHERE id =:idUser").setParameter("idUser", idUser).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public void deleteUserById(Long idUser) {
        try {
            em.createQuery("DELETE FROM UserDetail a WHERE a.id=:idUser").setParameter("idUser", idUser).executeUpdate();
        } catch (NoResultException e) {
            System.out.println(e);
        }
    }
}
