/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.basespring.dao;

import com.spring.basespring.model.UserData;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * @author Andri D Septian
 */
@Repository(value = "UserDataDao")
@Transactional
public class UserDataDao extends Dao{
    
    public UserData findUserByUsername(String username){
        try {
            UserData userdat = (UserData) em.createQuery("SELECT a FROM UserData a WHERE a.Username = :username")
                    .setParameter("username", username)
                    .getSingleResult();
            return userdat;
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<UserData> getAllUserData() {
        try {
            return (List<UserData>) em.createQuery("SELECT a FROM UserData a").getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    public UserData findUserById( Long idUser ) {
        try {
            return (UserData) em.createQuery("SELECT a FROM UserData a WHERE id =:idUser").setParameter("idUser", idUser).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
    
    public UserData saveOrUpdate(UserData userdat){
        if (userdat.getId() == null){
            em.persist(userdat);
            System.out.println("persist / saved");
        }
        else{
            em.merge(userdat);
            System.out.println("Merge / updated");
        }
        return userdat;
    }

    public void deleteUserById(Long idUser) {
        try {
            em.createQuery("DELETE FROM UserData a WHERE a.id=:idUser").setParameter("idUser", idUser).executeUpdate();
        } catch (NoResultException e) {
            System.out.println(e);
        }
    }
}
