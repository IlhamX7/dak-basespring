package com.spring.basespring.dao;

import com.spring.basespring.model.MhsData;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.NoResultException;
import java.util.List;

/**
 *
 * @author Ilham
 */
@Repository(value = "MhsDataDao")
@Transactional
public class MhsDataDao extends Dao{

    public MhsData findMhsByName(String Name){
        try {
            MhsData mhsdat = (MhsData) em.createQuery("SELECT a FROM MhsData a WHERE a.Name = :name")
                    .setParameter("name", Name)
                    .getSingleResult();
            return mhsdat;
        } catch (NoResultException e) {
            return null;
        }
    }

    public List<MhsData> getAllMhsData() {
        try {
            return (List<MhsData>) em.createQuery("SELECT a FROM MhsData a").getResultList();
        } catch (NoResultException e) {
            return null;
        }
    }

    public MhsData saveOrUpdate(MhsData mhsdat){
        if (mhsdat.getId() == null){
            em.persist(mhsdat);
            System.out.println("persist / saved");
        }
        else{
            em.merge(mhsdat);
            System.out.println("Merge / updated");
        }
        return mhsdat;
    }

    public MhsData findMhsById( Long idUser ) {
        try {
            return (MhsData) em.createQuery("SELECT a FROM MhsData a WHERE id =:idUser").setParameter("idUser", idUser).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public void deleteMhsById(Long idUser) {
        try {
            em.createQuery("DELETE FROM MhsData a WHERE a.id=:idUser").setParameter("idUser", idUser).executeUpdate();
        } catch (NoResultException e) {
            System.out.println(e);
        }
    }
}
